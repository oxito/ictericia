from fastapi import FastAPI, File, UploadFile
from image import Image
from fastapi import Response
from fastapi.middleware.cors import CORSMiddleware

app = FastAPI()

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.post("/")
async def create_upload_file(file: UploadFile):
    image = Image(await file.read())
    response = image.predict()
    if "error" in response:
        return response
    return {"normal": round(response[0]*100, 2), "jaundice": round(response[1]*100, 2)}

