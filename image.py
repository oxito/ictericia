import numpy as np
import cv2
from PIL import Image as PilImage
from io import BytesIO
import joblib
from sklearn.impute import SimpleImputer

class Image:
    def __init__(self, image=None):
        self.image = image

    def get_mean_color_channels(self, src, colors_space="rgb"):
        if colors_space == "rgb":
            image = cv2.cvtColor(src, cv2.COLOR_BGR2RGB)
        else:
            image = cv2.cvtColor(src, cv2.COLOR_BGR2YCrCb)

        return np.mean(cv2.split(image)[0]), np.mean(cv2.split(image)[1]), np.mean(cv2.split(image)[2])


    def convertToNumpy(self):
        return np.array(PilImage.open(BytesIO(self.image)))

    def generate_features(self):
        img = self.convertToNumpy()
        red, green, blue = self.get_mean_color_channels(img)
        y, cb, cr = self.get_mean_color_channels(img, colors_space="YCrCb")
        return np.array([[red, green, blue, y, cb, cr]])

    def extract_features(self):
        img = self.convertToNumpy()
        gray_image = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        moments = cv2.moments(gray_image)
        return -np.log(cv2.HuMoments(moments)).flatten()

    def is_valid(self):
        features = np.array(self.extract_features())
        imputer = SimpleImputer(strategy='constant', fill_value=0)
        instance = imputer.fit_transform(features.reshape(1, -1))
        image_validator = joblib.load("models/image_validator.pkl")
        return image_validator.predict(instance)

    def predict(self):
        # todo: gerar modelo caso não exista ainda.
        if self.is_valid() == 0:
            return {"error": "Image out of domain"}

        clf = joblib.load('models/jaundice.pkl')
        return clf.predict_proba(self.generate_features()).tolist()[0]